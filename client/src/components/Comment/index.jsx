/* eslint-disable no-shadow */
import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Comment as CommentUI, Icon, Form, Dropdown, Container } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { editComment, deleteComment, likeComment, dislikeComment } from 'src/containers/Thread/actions';

import styles from './styles.module.scss';

class Comment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            body: this.props.comment.body,
            editMode: false
        };
    }

    handleEditState = () => {
        this.setState({ editMode: true });
    }

    handleEditInput = (ev) => {
        this.setState({ body: ev.target.value });
    }

    handleEditComment = async (id) => {
        const { body } = this.state;
        if (!body) {
            return;
        }
        await this.props.editComment(id, { body });
        this.setState({ editMode: false });
    }

    handleDeleteComment = async (id) => {
        await this.props.deleteComment(id);
    }

    render() {
        const { comment: { createdAt, user, id, likeCount, dislikeCount, commentReactions },
            userId,
            likeComment,
            dislikeComment } = this.props;
        const { editMode } = this.state;
        const date = moment(createdAt).fromNow();
        return (
            <Container className={styles.commentWrapper}>
                <CommentUI className={styles.comment}>
                    <CommentUI.Avatar src={getUserImgLink(user.image)} />
                    <CommentUI.Content>
                        <CommentUI.Author as="a">
                            {user.username}
                        </CommentUI.Author>
                        <CommentUI.Metadata>
                            {date}
                        </CommentUI.Metadata>
                        {user.status
                            && (
                                <CommentUI.Metadata className={styles.statusBar}>
                                    {user.status}
                                </CommentUI.Metadata>
                            )
                        }
                        {
                            editMode
                                ? (
                                    <Form onSubmit={() => this.handleEditComment(id)}>
                                        <input
                                            name="edit"
                                            value={this.state.body}
                                            className={styles.editCommentInput}
                                            onChange={this.handleEditInput}
                                            // eslint-disable-next-line jsx-a11y/no-autofocus
                                            autoFocus
                                        />
                                    </Form>
                                )
                                : <CommentUI.Text>{this.state.body}</CommentUI.Text>
                        }
                        <CommentUI.Actions>
                            <CommentUI.Action onClick={() => likeComment(id)}>
                                <Icon name="thumbs up" size="small" />
                                <span className={styles.reactionQuantity}>{likeCount}</span>
                            </CommentUI.Action>
                            <CommentUI.Action onClick={() => dislikeComment(id)}>
                                <Icon name="thumbs down" size="small" />
                                <span className={styles.reactionQuantity}>{dislikeCount}</span>
                            </CommentUI.Action>
                            {
                                userId === user.id
                                && (editMode
                                    ? (
                                        <CommentUI.Action
                                            className={styles.editComment}
                                            onClick={() => this.handleEditComment(id)}
                                        >
                                            Save
                                        </CommentUI.Action>
                                    )
                                    : (
                                        <CommentUI.Action
                                            className={styles.editComment}
                                            onClick={this.handleEditState}
                                        >
                                    Edit
                                        </CommentUI.Action>
                                    ))
                            }
                            {
                                userId === user.id
                                && (
                                    <CommentUI.Action
                                        className={styles.deleteComment}
                                        onClick={() => this.handleDeleteComment(id)}
                                    >
                                Delete
                                    </CommentUI.Action>
                                )
                            }
                        </CommentUI.Actions>
                    </CommentUI.Content>
                </CommentUI>
                <Container className={styles.likeListDropdown}>
                    {commentReactions.length
                        ? (
                            <Dropdown text="Liked by">
                                <Dropdown.Menu>
                                    {commentReactions.map((el, i) => el.isLike
                                    // eslint-disable-next-line react/no-array-index-key
                                    && <Dropdown.Item text={el.user.username} key={i} />)}
                                </Dropdown.Menu>
                            </Dropdown>
                        )
                        : <div />
                    }
                </Container>
            </Container>
        );
    }
}

Comment.propTypes = {
    comment: PropTypes.objectOf(PropTypes.any).isRequired,
    userId: PropTypes.string.isRequired,
    editComment: PropTypes.func.isRequired,
    deleteComment: PropTypes.func.isRequired,
    likeComment: PropTypes.func.isRequired,
    dislikeComment: PropTypes.func.isRequired
};


const actions = { editComment, deleteComment, likeComment, dislikeComment };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    null,
    mapDispatchToProps
)(Comment);
