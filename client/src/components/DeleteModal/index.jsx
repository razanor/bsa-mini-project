import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button, Header, Icon } from 'semantic-ui-react';

const state = {
    open: true
};

const DeleteModal = ({ closeDeletedPost, handleDeletePost }) => (
    <Modal dimmer="blurring" centered={false} open={state.open} basic size="small" onClose={() => closeDeletedPost()}>
        <Header icon="delete" content="Delete post" />
        <Modal.Content>
            <p>
                Are you sure you want to delete this post?
            </p>
        </Modal.Content>
        <Modal.Actions>
            <Button basic color="red" inverted onClick={() => closeDeletedPost()}>
                <Icon name="remove" />
                {' '}
                No
            </Button>
            <Button color="green" inverted onClick={() => handleDeletePost()}>
                <Icon name="checkmark" />
                {' '}
                Yes
            </Button>
        </Modal.Actions>
    </Modal>
);


DeleteModal.propTypes = {
    handleDeletePost: PropTypes.func.isRequired,
    closeDeletedPost: PropTypes.func.isRequired
};

export default DeleteModal;
