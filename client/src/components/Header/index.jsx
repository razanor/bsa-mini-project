/* eslint-disable max-len */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Header as HeaderUI, Image, Grid, Icon, Button, Container, Form } from 'semantic-ui-react';
import { updateUserDetails } from '../../containers/Profile/actions';

import styles from './styles.module.scss';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            status: this.props.user.status,
            editMode: false
        };
    }

    componentDidUpdate(prevProps) {
        if (this.props.user.status !== prevProps.user.status) {
            // eslint-disable-next-line react/no-did-update-set-state
            this.setState({ status: this.props.user.status });
        }
    }

    handleEditStatus = async () => {
        const { id } = this.props.user;
        const { status } = this.state;
        await this.props.updateUserDetails(id, { status });
        this.setState({ editMode: false });
    }

    render() {
        const { user, logout } = this.props;
        const { editMode, status } = this.state;
        return (
            <div className={styles.headerWrp}>
                <Grid centered container columns="2">
                    <Grid.Column>
                        {user && (
                            <Container>
                                <NavLink exact to="/profile">
                                    <HeaderUI>
                                        <Image circular src={getUserImgLink(user.image)} />
                                        {' '}
                                        {user.username}
                                    </HeaderUI>
                                </NavLink>
                                {user.status && (
                                    editMode
                                        ? (
                                            <Form onSubmit={() => this.handleEditStatus()} className={styles.editStatusForm}>
                                                <input
                                                    className={styles.editStatusInput}
                                                    name="edit"
                                                    value={status}
                                                    onChange={ev => this.setState({ status: ev.target.value })}
                                                    // eslint-disable-next-line jsx-a11y/no-autofocus
                                                    autoFocus
                                                />
                                            </Form>
                                        )
                                        : (
                                            <p
                                                className={styles.status}
                                                onClick={() => this.setState({ editMode: !editMode })}
                                            >
                                                {user.status}
                                            </p>
                                        )
                                )}
                            </Container>
                        )}
                    </Grid.Column>
                    <Grid.Column textAlign="right">
                        <NavLink exact activeClassName="active" to="/">
                            <Icon name="home" size="large" />
                        </NavLink>
                        <Button basic icon type="button" className={styles.logoutBtn} onClick={logout}>
                            <Icon name="log out" size="large" />
                        </Button>
                    </Grid.Column>
                </Grid>
            </div>
        );
    }
}

Header.propTypes = {
    logout: PropTypes.func.isRequired,
    user: PropTypes.objectOf(PropTypes.any).isRequired,
    updateUserDetails: PropTypes.func.isRequired
};


const actions = { updateUserDetails };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    null,
    mapDispatchToProps
)(Header);
