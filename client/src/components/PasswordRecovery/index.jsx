import React from 'react';
import { Redirect, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import validator from 'validator';
import Logo from 'src/components/Logo';
import { recoverPassword } from 'src/services/authService';

import {
    Grid,
    Header,
    Form,
    Button,
    Segment,
    Message
} from 'semantic-ui-react';

class RecoverPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            isLoading: false,
            isEmailValid: true,
        };
    }

    validateEmail = () => {
        const { email } = this.state;
        const isEmailValid = !validator.isEmpty(email);
        this.setState({ isEmailValid });
        return isEmailValid;
    };

    handleRecoverPassword = async () => {
        const { isLoading, email } = this.state;
        const valid = this.validateForm();
        if (!valid || isLoading) {
            return;
        }
        const emailSend = await recoverPassword({ email });
        console.log(emailSend); // to do
    }

    emailChanged = email => this.setState({ email, isEmailValid: true });

    validateForm = () => [
        this.validateEmail()
    ].every(Boolean);

    render() {
        const { isEmailValid, isLoading } = this.state;
        return !this.props.isAuthorized
            ? (
                <Grid textAlign="center" verticalAlign="middle" className="fill">
                    <Grid.Column style={{ maxWidth: 450 }}>
                        <Logo />
                        <Header as="h2" color="teal" textAlign="center">
                            Recover your password
                        </Header>
                        <Form name="loginForm" size="large" onSubmit={this.handleRecoverPassword}>
                            <Segment>
                                <Form.Input
                                    fluid
                                    icon="at"
                                    iconPosition="left"
                                    placeholder="Email"
                                    type="email"
                                    error={!isEmailValid}
                                    onChange={ev => this.emailChanged(ev.target.value)}
                                    onBlur={this.validateEmail}
                                />
                                <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
                                    Submit
                                </Button>
                            </Segment>
                        </Form>
                        <Message>
                            <NavLink exact to="/login">Go back</NavLink>
                        </Message>
                    </Grid.Column>
                </Grid>
            )
            : <Redirect to="/" />;
    }
}

RecoverPassword.propTypes = {
    isAuthorized: PropTypes.bool
};

RecoverPassword.defaultProps = {
    isAuthorized: false
};

export default RecoverPassword;
