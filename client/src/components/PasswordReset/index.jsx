import React from 'react';
import { Redirect, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import Logo from 'src/components/Logo';

import {
    Grid,
    Header,
    Form,
    Button,
    Segment,
    Message
} from 'semantic-ui-react';

class PasswordReset extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
        };
    }

    render() {
        return !this.props.isAuthorized
            ? (
                <Grid textAlign="center" verticalAlign="middle" className="fill">
                    <Grid.Column style={{ maxWidth: 450 }}>
                        <Logo />
                        <Header as="h2" color="teal" textAlign="center">
                            Reset your password
                        </Header>
                        <Form name="loginForm" size="large" onSubmit={this.handleRecoverPassword}>
                            <Segment>
                                <Form.Input
                                    fluid
                                    icon="lock"
                                    iconPosition="left"
                                    placeholder="New password"
                                    value={this.state.password}
                                    type="password"
                                    onChange={ev => this.setState({ password: ev.target.value })}
                                    onBlur={this.validateEmail}
                                />
                                <Button type="submit" color="teal" fluid size="large" primary>
                                    Reset
                                </Button>
                            </Segment>
                        </Form>
                        <Message>
                            <NavLink exact to="/">Go home</NavLink>
                        </Message>
                    </Grid.Column>
                </Grid>
            )
            : <Redirect to="/" />;
    }
}

PasswordReset.propTypes = {
    isAuthorized: PropTypes.bool
};

PasswordReset.defaultProps = {
    isAuthorized: false
};

export default PasswordReset;
