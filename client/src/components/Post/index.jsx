import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Dropdown } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';

const Post = ({ post, likePost, dislikePost, toggleExpandedPost,
    toggleEditedPost, toggleDeletedPost, sharePost, userId }) => {
    const {
        id,
        image,
        body,
        user,
        likeCount,
        dislikeCount,
        commentCount,
        createdAt,
        postReactions
    } = post;

    let editIcon;
    let deleteIcon;
    if (user.id === userId) {
        editIcon = (
            <Label
                basic
                size="medium"
                as="a"
                attached="bottom right"
                className={styles.toolbarBtn}
                onClick={() => toggleEditedPost(id)}
            >
                <Icon name="edit" />
            </Label>
        );
        deleteIcon = (
            <Label
                basic
                size="big"
                color="red"
                as="a"
                horizontal
                className={styles.deleteBtn}
                onClick={() => toggleDeletedPost(id)}
            >
                <Icon name="delete" />
            </Label>
        );
    }
    const date = moment(createdAt).fromNow();
    return (
        <Card style={{ width: '100%' }}>
            {image && <Image src={image.link} wrapped ui={false} />}
            <Card.Content>
                <Card.Meta>
                    <span className="date">
                        posted by
                        {' '}
                        {user.username}
                        {' - '}
                        {date}
                    </span>
                    {deleteIcon}
                </Card.Meta>
                <Card.Description>
                    {body}
                </Card.Description>
            </Card.Content>
            <Card.Content extra>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
                    <Icon name="thumbs up" />
                    {likeCount}
                </Label>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
                    <Icon name="thumbs down" />
                    {dislikeCount}
                </Label>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
                    <Icon name="comment" />
                    {commentCount}
                </Label>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
                    <Icon name="share alternate" />
                </Label>
                {editIcon}
                { postReactions.length
                    ? (
                        <Dropdown text="Liked by">
                            <Dropdown.Menu>
                                {postReactions.map((el, i) => el.isLike
                                    // eslint-disable-next-line react/no-array-index-key
                                    && <Dropdown.Item text={el.user.username} key={i} />)}
                            </Dropdown.Menu>
                        </Dropdown>
                    )
                    : <div />
                }
            </Card.Content>
        </Card>
    );
};


Post.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    likePost: PropTypes.func.isRequired,
    dislikePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    toggleEditedPost: PropTypes.func,
    toggleDeletedPost: PropTypes.func,
    sharePost: PropTypes.func.isRequired,
    userId: PropTypes.string
};

Post.defaultProps = {
    userId: undefined,
    toggleEditedPost: undefined,
    toggleDeletedPost: undefined
};

export default Post;
