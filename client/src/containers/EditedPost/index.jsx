import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Header, Form, Button, Icon, Image, Segment } from 'semantic-ui-react';
import { toggleEditedPost, editPost } from 'src/containers/Thread/actions';
import Spinner from 'src/components/Spinner';

import styles from '../../components/AddPost/styles.module.scss';

class EditedPost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: true,
            body: this.props.post.body,
            imageId: this.props.post.image ? this.props.post.image.id : undefined,
            imageLink: this.props.post.image ? this.props.post.image.link : undefined,
            isUploading: false
        };
    }

    closeModal = () => {
        this.props.toggleEditedPost();
    }

    handleEditPost = async () => {
        const { imageId, body } = this.state;
        if (!body) {
            return;
        }
        const postId = this.props.post.id;
        await this.props.editPost(postId, { imageId, body });
        this.closeModal();
    }

    handleUploadFile = async ({ target }) => {
        this.setState({ isUploading: true });
        try {
            const { id: imageId, link: imageLink } = await this.props.uploadImage(target.files[0]);
            this.setState({ imageId, imageLink, isUploading: false });
        } catch {
            // TODO: show error
            this.setState({ isUploading: false });
        }
    }

    render() {
        const { post } = this.props;
        const { imageLink, body, isUploading } = this.state;
        return (
            <Modal dimmer="blurring" centered={false} open={this.state.open} onClose={this.closeModal}>
                {post
                    ? (
                        <Segment>
                            <Header as="h3" dividing>
                                Edit post
                            </Header>
                            <Form onSubmit={this.handleEditPost}>
                                <Form.TextArea
                                    name="body"
                                    value={body}
                                    placeholder="What is the news?"
                                    onChange={ev => this.setState({ body: ev.target.value })}
                                    rows={7}
                                />
                                {imageLink && (
                                    <div className={styles.imageWrapper}>
                                        <Image className={styles.image} src={imageLink} alt="post" />
                                    </div>
                                )}
                                <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
                                    <Icon name="image" />
                                    Attach image
                                    <input name="image" type="file" onChange={this.handleUploadFile} hidden />
                                </Button>
                                <Button floated="right" color="blue" type="submit">Edit</Button>
                            </Form>
                        </Segment>
                    )
                    : <Spinner />
                }
            </Modal>
        );
    }
}

EditedPost.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    toggleEditedPost: PropTypes.func.isRequired,
    editPost: PropTypes.func.isRequired,
    uploadImage: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
    post: rootState.posts.editedPost
});
const actions = { toggleEditedPost, editPost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditedPost);
