import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getUserImgLink, getCroppedImg, urlToFile } from 'src/helpers/imageHelper';
import { Grid, Image, Input, Form, Button, Message, TextArea } from 'semantic-ui-react';
import * as imageService from 'src/services/imageService';
import ReactCrop from 'react-image-crop';
import { updateUserDetails } from './actions';

import 'react-image-crop/lib/ReactCrop.scss';
import styles from './styles.module.scss';

class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: this.props.user.username,
            status: this.props.user.status,
            isUploading: false,
            isSubmitted: false,
            crop: {
                aspect: 1 / 1,
                maxWidth: 200,
                maxHeight: 200
            },
            toCropImage: undefined,
            croppedImageUrl: undefined
        };
    }

    componentDidUpdate(prevProps) {
        if (this.props.user.status !== prevProps.user.status) {
            // eslint-disable-next-line react/no-did-update-set-state
            this.setState({ status: this.props.user.status });
        }
    }

    onImageLoaded = (image) => {
        this.imageRef = image;
    };

    onCropComplete = (crop) => {
        this.makeClientCrop(crop);
    };

    handleCropImage = ({ target }) => {
        if (target.files && target.files.length > 0) {
            this.setState({ isSubmitted: false });
            const reader = new FileReader();
            reader.addEventListener('load', () => {
                this.setState({ toCropImage: reader.result });
            });
            reader.readAsDataURL(target.files[0]);
        }
    }


    handleUpdateUser = async () => {
        const { username, croppedImageUrl, status } = this.state;
        let imageId;
        if (croppedImageUrl) {
            this.setState({ isUploading: true });
            const file = await urlToFile(croppedImageUrl, 'test.png');
            const croppedImage = await this.uploadImage(file);
            imageId = croppedImage.id;
        }
        const { id: userId } = this.props.user;
        if (!username) {
            return;
        }
        await this.props.updateUserDetails(userId, { imageId, username, status });
        this.setState({
            isSubmitted: true,
            toCropImage: undefined,
            isUploading: false
        });
    };

    uploadImage = file => imageService.uploadImage(file);

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await getCroppedImg(
                this.imageRef,
                crop,
                'newFile.jpeg',
                this.fileUrl
            );
            this.setState({ croppedImageUrl });
        }
    }

    render() {
        const { user } = this.props;
        const {
            username,
            status,
            isUploading,
            isSubmitted,
            croppedImageUrl,
            toCropImage } = this.state;
        return (
            <Grid container textAlign="center" style={{ paddingTop: 30 }}>
                <Grid.Column>
                    <Form
                        success={isSubmitted}
                        className={styles.form}
                        onSubmit={this.handleUpdateUser}
                    >
                        <Button as="label" style={{ background: 'none' }} disabled={isUploading}>
                            <Image
                                centered
                                src={croppedImageUrl || getUserImgLink(user.image)}
                                size="medium"
                                circular
                            />
                            <input name="image" type="file" onChange={this.handleCropImage} hidden />
                        </Button>
                        { toCropImage
                            && (
                                <ReactCrop
                                    style={{ width: '30%' }}
                                    src={toCropImage}
                                    crop={this.state.crop}
                                    onImageLoaded={this.onImageLoaded}
                                    onComplete={this.onCropComplete}
                                    onChange={crop => this.setState({ crop })}
                                />
                            )
                        }
                        <br />
                        <Form.Field>
                            <Input
                                icon="user"
                                iconPosition="left"
                                placeholder="Username"
                                type="text"
                                value={username}
                                onChange={ev => this.setState({ username: ev.target.value })}
                            />
                        </Form.Field>
                        <Form.Field>
                            <Input
                                icon="at"
                                iconPosition="left"
                                placeholder="Email"
                                type="email"
                                disabled
                                value={user.email}
                            />
                        </Form.Field>
                        <Form.Field>
                            <TextArea
                                placeholder="status"
                                value={status || ''}
                                onChange={ev => this.setState({ status: ev.target.value })}
                            />
                        </Form.Field>
                        <Button type="submit" color="green">Update</Button>
                        <Message success header="Your information has been updated" />
                    </Form>
                </Grid.Column>
            </Grid>
        );
    }
}

Profile.propTypes = {
    user: PropTypes.objectOf(PropTypes.any),
    updateUserDetails: PropTypes.func.isRequired
};

Profile.defaultProps = {
    user: {}
};

const mapStateToProps = rootState => ({
    user: rootState.profile.user
});

const actions = {
    updateUserDetails
};
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile);
