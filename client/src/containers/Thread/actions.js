/* eslint-disable max-len */

import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
    ADD_POST,
    EDIT_POST,
    DELETE_POST,
    LOAD_MORE_POSTS,
    SET_ALL_POSTS,
    SET_EXPANDED_POST,
    SET_EDITED_POST
} from './actionTypes';

const setPostsAction = posts => ({
    type: SET_ALL_POSTS,
    posts
});

const addMorePostsAction = posts => ({
    type: LOAD_MORE_POSTS,
    posts
});

const addPostAction = post => ({
    type: ADD_POST,
    post
});

const editPostAction = post => ({
    type: EDIT_POST,
    post
});

const deletePostAction = posts => ({
    type: DELETE_POST,
    posts
});

const setExpandedPostAction = post => ({
    type: SET_EXPANDED_POST,
    post
});

const setEditedPostAction = post => ({
    type: SET_EDITED_POST,
    post
});

export const loadPosts = filter => async (dispatch) => {
    const posts = await postService.getAllPosts(filter);
    dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
    const { posts: { posts } } = getRootState();
    const loadedPosts = await postService.getAllPosts(filter);
    const filteredPosts = loadedPosts
        .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
    dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async (dispatch) => {
    const post = await postService.getPost(postId);
    dispatch(addPostAction(post));
};

export const addPost = post => async (dispatch) => {
    const { id } = await postService.addPost(post);
    const newPost = await postService.getPost(id);
    dispatch(addPostAction(newPost));
};

export const editPost = (postId, post) => async (dispatch) => {
    await postService.editPost(postId, post);
    const editedPost = await postService.getPost(postId);
    dispatch(editPostAction(editedPost));
};

export const deletePost = postId => async (dispatch) => {
    await postService.deletePost(postId);
    const postsList = await postService.getAllPosts({
        userId: undefined,
        from: 0,
        count: 10
    });
    dispatch(deletePostAction(postsList));
};

export const toggleExpandedPost = postId => async (dispatch) => {
    const post = postId ? await postService.getPost(postId) : undefined;
    dispatch(setExpandedPostAction(post));
};

export const toggleEditedPost = postId => async (dispatch) => {
    const post = postId ? await postService.getPost(postId) : undefined;
    dispatch(setEditedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
    const { id } = await postService.likePost(postId);
    const { dislikeCount, postReactions } = await postService.getPost(postId);
    const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

    const mapLikes = post => ({
        ...post,
        likeCount: Number(post.likeCount) + diff, // diff is taken from the current closure
        dislikeCount,
        postReactions
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapLikes(expandedPost)));
    }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
    const { id } = await postService.dislikePost(postId);
    const { likeCount, postReactions } = await postService.getPost(postId);
    const diff = id ? 1 : -1; // if ID exists then the post was disliked, otherwise - dislike was removed

    const mapDislikes = post => ({
        ...post,
        dislikeCount: Number(post.dislikeCount) + diff, // diff is taken from the current closure
        likeCount,
        postReactions
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
    }
};

export const addComment = request => async (dispatch, getRootState) => {
    const { id } = await commentService.addComment(request);
    const comment = await commentService.getComment(id);

    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) + 1,
        comments: [...(post.comments || []), comment] // comment is taken from the current closure
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== comment.postId
        ? post
        : mapComments(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const editComment = (id, comment) => async (dispatch, getRootState) => {
    await commentService.editComment(id, comment);
    const updatedComment = await commentService.getComment(id);

    const mapComments = post => ({
        ...post,
        comments: [...(post.comments || []), updatedComment] // comment is taken from the current closure
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== comment.postId
        ? post
        : mapComments(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const deleteComment = id => async (dispatch, getRootState) => {
    await commentService.deleteComment(id);

    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) - 1,
    });

    const { posts: { posts, expandedPost } } = getRootState();

    const updatedExpanded = Object.assign({}, expandedPost);
    updatedExpanded.comments = updatedExpanded.comments.filter(el => el.id !== id);

    const updatedPosts = posts.map(post => (post.id === expandedPost.id
        ? mapComments(post)
        : post));

    dispatch(setPostsAction(updatedPosts));

    dispatch(setExpandedPostAction(mapComments(updatedExpanded)));
};

export const likeComment = commentId => async (dispatch, getRootState) => {
    const { id } = await commentService.likeComment(commentId);
    const { dislikeCount, commentReactions } = await commentService.getComment(commentId);
    const diff = id ? 1 : -1; // if ID exists then the comment was liked, otherwise - like was removed

    const mapLikes = comment => ({
        ...comment,
        likeCount: Number(comment.likeCount) + diff, // diff is taken from the current closure
        dislikeCount,
        commentReactions
    });

    const { posts: { expandedPost } } = getRootState();

    const updated = Object.assign({}, expandedPost);
    updated.comments = updated.comments.map(comment => (comment.id === commentId
        ? mapLikes(comment)
        : comment
    ));

    dispatch(setExpandedPostAction(updated));
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
    const { id } = await commentService.dislikeComment(commentId);
    const { likeCount, commentReactions } = await commentService.getComment(commentId);
    const diff = id ? 1 : -1; // if ID exists then the comment was liked, otherwise - like was removed

    const mapDisLikes = comment => ({
        ...comment,
        dislikeCount: Number(comment.dislikeCount) + diff, // diff is taken from the current closure
        likeCount,
        commentReactions
    });

    const { posts: { expandedPost } } = getRootState();

    const updated = Object.assign({}, expandedPost);
    updated.comments = updated.comments.map(comment => (comment.id === commentId
        ? mapDisLikes(comment)
        : comment
    ));

    dispatch(setExpandedPostAction(updated));
};
