import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import EditedPost from 'src/containers/EditedPost';
import DeleteModal from 'src/components/DeleteModal/';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import { loadPosts, loadMorePosts, likePost, dislikePost, toggleExpandedPost, deletePost, toggleEditedPost, addPost } from './actions';

import styles from './styles.module.scss';

class Thread extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sharedPostId: undefined,
            deletedPostId: undefined,
            showOwnPosts: false,
            hideOwnPosts: false,
            showLikedByMe: false
        };
        this.postsFilter = {
            userId: this.props.userId,
            showOwnPosts: undefined,
            hideOwnPosts: undefined,
            showLikedByMe: undefined,
            from: 0,
            count: 10
        };
    }

    togglePosts = () => {
        this.setState(
            ({ showOwnPosts }) => ({ showOwnPosts: !showOwnPosts }),
            () => {
                Object.assign(this.postsFilter, {
                    showOwnPosts: this.state.showOwnPosts ? true : undefined,
                    from: 0
                });
                this.props.loadPosts(this.postsFilter);
                this.postsFilter.from = this.postsFilter.count; // for next scroll
            }
        );
    };

    toggleHideOwnPosts = () => {
        this.setState(
            ({ hideOwnPosts }) => ({ hideOwnPosts: !hideOwnPosts }),
            () => {
                Object.assign(this.postsFilter, {
                    hideOwnPosts: this.state.hideOwnPosts ? true : undefined,
                    from: 0
                });
                this.props.loadPosts(this.postsFilter);
                this.postsFilter.from = this.postsFilter.count; // for next scroll
            }
        );
    };

    toggleLikedByMe = () => {
        this.setState(
            ({ showLikedByMe }) => ({ showLikedByMe: !showLikedByMe }),
            () => {
                Object.assign(this.postsFilter, {
                    showLikedByMe: this.state.showLikedByMe ? true : undefined,
                    from: 0
                });
                this.props.loadPosts(this.postsFilter);
                this.postsFilter.from = this.postsFilter.count; // for next scroll
            }
        );
    };

    loadMorePosts = () => {
        this.props.loadMorePosts(this.postsFilter);
        const { from, count } = this.postsFilter;
        this.postsFilter.from = from + count;
    }

    sharePost = (sharedPostId) => {
        this.setState({ sharedPostId });
    };

    closeSharePost = () => {
        this.setState({ sharedPostId: undefined });
    }

    toggleDeletedPost = (deletedPostId) => {
        this.setState({ deletedPostId });
    }

    handleDeletePost = async () => {
        const { deletedPostId } = this.state;
        await this.props.deletePost(deletedPostId);
        this.closeDeletedPost();
    }

    closeDeletedPost = () => {
        this.setState({ deletedPostId: undefined });
    }

    uploadImage = file => imageService.uploadImage(file);

    render() {
        const { posts = [], expandedPost, editedPost, hasMorePosts, ...props } = this.props;
        const { showOwnPosts, sharedPostId,
            deletedPostId, hideOwnPosts, showLikedByMe } = this.state;
        return (
            <div className={styles.threadContent}>
                <div className={styles.addPostForm}>
                    <AddPost addPost={props.addPost} uploadImage={this.uploadImage} />
                </div>
                <div className={styles.toolbar}>
                    <Checkbox toggle label="Show only my posts" disabled={hideOwnPosts} checked={showOwnPosts} onChange={this.togglePosts} />
                </div>
                <div className={styles.toolbarRadio}>
                    <Checkbox label="Hide own posts" disabled={showOwnPosts} checked={hideOwnPosts} onChange={this.toggleHideOwnPosts} />
                </div>
                <div className={styles.toolbarRadio}>
                    <Checkbox label="Show posts liked by me" checked={showLikedByMe} onChange={this.toggleLikedByMe} />
                </div>
                <InfiniteScroll
                    pageStart={0}
                    loadMore={this.loadMorePosts}
                    hasMore={hasMorePosts}
                    loader={<Loader active inline="centered" key={0} />}
                >
                    {posts.map(post => (
                        <Post
                            post={post}
                            likePost={props.likePost}
                            dislikePost={props.dislikePost}
                            toggleExpandedPost={props.toggleExpandedPost}
                            toggleEditedPost={props.toggleEditedPost}
                            toggleDeletedPost={this.toggleDeletedPost}
                            sharePost={this.sharePost}
                            key={post.id}
                            userId={props.userId}
                        />
                    ))}
                </InfiniteScroll>
                {
                    expandedPost
                    && <ExpandedPost sharePost={this.sharePost} userId={props.userId} />
                }
                {
                    sharedPostId
                    && <SharedPostLink postId={sharedPostId} close={this.closeSharePost} />
                }
                {
                    editedPost
                    && <EditedPost uploadImage={this.uploadImage} />
                }
                {
                    deletedPostId
                    && (
                        <DeleteModal
                            closeDeletedPost={this.closeDeletedPost}
                            handleDeletePost={this.handleDeletePost}
                        />
                    )
                }
            </div>
        );
    }
}

Thread.propTypes = {
    posts: PropTypes.arrayOf(PropTypes.object),
    hasMorePosts: PropTypes.bool,
    expandedPost: PropTypes.objectOf(PropTypes.any),
    editedPost: PropTypes.objectOf(PropTypes.any),
    deletedPostId: PropTypes.string,
    sharedPostId: PropTypes.string,
    userId: PropTypes.string,
    loadPosts: PropTypes.func.isRequired,
    loadMorePosts: PropTypes.func.isRequired,
    likePost: PropTypes.func.isRequired,
    dislikePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    toggleEditedPost: PropTypes.func.isRequired,
    addPost: PropTypes.func.isRequired,
    deletePost: PropTypes.func.isRequired
};

Thread.defaultProps = {
    posts: [],
    hasMorePosts: true,
    expandedPost: undefined,
    editedPost: undefined,
    sharedPostId: undefined,
    deletedPostId: undefined,
    userId: undefined
};

const mapStateToProps = rootState => ({
    posts: rootState.posts.posts,
    hasMorePosts: rootState.posts.hasMorePosts,
    expandedPost: rootState.posts.expandedPost,
    editedPost: rootState.posts.editedPost,
    userId: rootState.profile.user.id
});

const actions = {
    loadPosts,
    loadMorePosts,
    likePost,
    dislikePost,
    toggleExpandedPost,
    toggleEditedPost,
    addPost,
    deletePost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Thread);
