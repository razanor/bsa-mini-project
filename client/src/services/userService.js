import callWebApi from 'src/helpers/webApiHelper';

export const editUser = async (id, request) => {
    const response = await callWebApi({
        endpoint: `/api/users/${id}`,
        type: 'PUT',
        request
    });
    return response.json();
};
