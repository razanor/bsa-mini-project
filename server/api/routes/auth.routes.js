import { Router } from 'express';
import * as authService from '../services/auth.service';
import * as userService from '../services/user.service';
import authenticationMiddleware from '../middlewares/authentication.middleware';
import registrationMiddleware from '../middlewares/registration.middleware';
import jwtMiddleware from '../middlewares/jwt.middleware';
import { sendEmail } from '../middlewares/email.middleware';

const router = Router();

router
    .post('/password-recovery', (req, res, next) => authService.setRecoveryToken(req.body.email)
        .then((token) => {
            if (token) {
                const toEmail = req.body.email;
                const message = `<p>Please visit this link to recover your password 
                <strong>http://localhost:3001/reset/${token.resetPasswordToken}</strong>`;
                const subject = 'Thread | Recover password';
                sendEmail(toEmail, message, subject);
            }
        })
        .catch(next))
    .post('/login', authenticationMiddleware, (req, res, next) => authService.login(req.user) // user added to the request in the login strategy, see passport config
        .then(data => res.send(data))
        .catch(next))
    .post('/register', registrationMiddleware, (req, res, next) => authService.register(req.user) // user added to the request in the register strategy, see passport config
        .then(data => res.send(data))
        .catch(next))
    .get('/user', jwtMiddleware, (req, res, next) => userService.getUserById(req.user.id) // user added to the request in the jwt strategy, see passport config
        .then(data => res.send(data))
        .catch(next));

export default router;
