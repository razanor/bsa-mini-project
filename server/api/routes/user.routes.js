import { Router } from 'express';
import * as userService from '../services/user.service';

const router = Router();

router
    .put('/:id', (req, res, next) => userService.editUser(req.params.id, req.body)
        .then(user => res.send(user))
        .catch(next));

export default router;
