import sequelize from '../db/connection';
import { CommentModel, UserModel, ImageModel, CommentReactionModel } from '../models/index';
import BaseRepository from './base.repository';

const likeCase = bool => `(SELECT COALESCE(SUM(CASE WHEN "reaction"."isLike" = ${bool} THEN 1 ELSE 0 END), 0) 
FROM "commentReactions" as "reaction" 
WHERE "comment"."id" = "reaction"."commentId")`;

class CommentRepository extends BaseRepository {
    getCommentById(id) {
        return this.model.findOne({
            group: [
                'comment.id',
                'user.id',
                'user->image.id',
                'commentReactions.id',
                'commentReactions->user.id'
            ],
            where: { id },
            attributes: {
                include: [
                    [sequelize.literal(likeCase(true)), 'likeCount'],
                    [sequelize.literal(likeCase(false)), 'dislikeCount'],
                ]
            },
            include: [{
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            },
            {
                model: CommentReactionModel,
                attributes: ['userId', 'isLike'],
                include: {
                    model: UserModel,
                    attributes: ['username']
                }
            }]
        });
    }
}

export default new CommentRepository(CommentModel);
