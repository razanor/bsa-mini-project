import { PostReactionModel, PostModel, UserModel } from '../models/index';
import BaseRepository from './base.repository';

class PostReactionRepository extends BaseRepository {
    getPostReaction(userId, postId) {
        return this.model.findOne({
            group: [
                'postReaction.id',
                'post.id',
                'user.id',
                'post->user.id'
            ],
            where: { userId, postId },
            include: [{
                model: PostModel,
                attributes: ['id', 'userId'],
                include: {
                    model: UserModel,
                    attributes: ['email']
                }
            }, {
                model: UserModel,
                attributes: ['username']
            }]
        });
    }
}

export default new PostReactionRepository(PostReactionModel);
