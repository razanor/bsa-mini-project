import Sequelize from 'sequelize';
import sequelize from '../db/connection';
import { PostModel, CommentModel, UserModel, ImageModel, PostReactionModel, CommentReactionModel } from '../models/index';
import BaseRepository from './base.repository';

const likeCase = bool => `(SELECT COALESCE(SUM(CASE WHEN "reaction"."isLike" = ${bool} THEN 1 ELSE 0 END), 0) 
FROM "postReactions" as "reaction" 
WHERE "post"."id" = "reaction"."postId")`;

const likeCommentCase = bool => `(SELECT COALESCE(SUM(CASE WHEN "reaction"."isLike" = ${bool} THEN 1 ELSE 0 END), 0) 
FROM "commentReactions" as "reaction" 
WHERE "comments"."id" = "reaction"."commentId")`;

class PostRepository extends BaseRepository {
    async getPosts(filter) {
        const {
            from: offset,
            count: limit,
            userId,
            showOwnPosts,
            hideOwnPosts,
            showLikedByMe,
        } = filter;

        const where = {};
        if (showOwnPosts) {
            Object.assign(where, { userId });
        }

        if (hideOwnPosts) {
            Object.assign(where, {
                userId: {
                    [Sequelize.Op.not]: userId,
                }
            });
        }

        return this.model.findAll({
            where,
            attributes: {
                include: [
                    [sequelize.literal(`
                            (SELECT COUNT(*)
                            FROM "comments" as "comment"
                            WHERE "post"."id" = "comment"."postId" 
                            AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
                    [sequelize.literal(likeCase(true)), 'likeCount'],
                    [sequelize.literal(likeCase(false)), 'dislikeCount']
                ]
            },
            include: [{
                model: ImageModel,
                attributes: ['id', 'link']
            }, {
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }, {
                model: PostReactionModel,
                where: showLikedByMe ? { isLike: true, userId }
                    : undefined, // When object is empty strange behavior occur, hence undef used
                attributes: [],
                duplicating: false
            }, {
                model: PostReactionModel,
                attributes: ['userId', 'isLike'],
                include: {
                    model: UserModel,
                    attributes: ['username']
                }
            }],
            group: [
                'post.id',
                'image.id',
                'user.id',
                'user->image.id',
                'postReactions.id',
                'postReactions->user.id'
            ],
            order: [['createdAt', 'DESC']],
            offset,
            limit
        });
    }

    getPostById(id) {
        return this.model.findOne({
            group: [
                'post.id',
                'comments.id',
                'comments->user.id',
                'comments->user->image.id',
                'user.id',
                'user->image.id',
                'image.id',
                'postReactions.id',
                'postReactions->user.id',
                'comments->commentReactions',
                'comments->commentReactions.id',
                'comments->commentReactions->user.id'

            ],
            where: { id },
            attributes: {
                include: [
                    [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
                    [sequelize.literal(likeCase(true)), 'likeCount'],
                    [sequelize.literal(likeCase(false)), 'dislikeCount']
                ]
            },
            include: [{
                model: CommentModel,
                attributes: {
                    include: [
                        [sequelize.literal(likeCommentCase(true)), 'likeCount'],
                        [sequelize.literal(likeCommentCase(false)), 'dislikeCount']
                    ]
                },
                include: [{
                    model: UserModel,
                    attributes: ['id', 'username', 'status'],
                    include: {
                        model: ImageModel,
                        attributes: ['id', 'link']
                    }
                },
                {
                    model: CommentReactionModel,
                    attributes: ['userId', 'isLike'],
                    include: {
                        model: UserModel,
                        attributes: ['username']
                    }
                }]
            }, {
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }, {
                model: ImageModel,
                attributes: ['id', 'link']
            }, {
                model: PostReactionModel,
                attributes: ['userId', 'isLike'],
                include: {
                    model: UserModel,
                    attributes: ['username']
                }
            }]
        });
    }
}

export default new PostRepository(PostModel);
