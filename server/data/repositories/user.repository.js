import crypto from 'crypto';
import { UserModel, ImageModel } from '../models/index';
import BaseRepository from './base.repository';

class UserRepository extends BaseRepository {
    addUser(user) {
        return this.create(user);
    }

    getByEmail(email) {
        return this.model.findOne({ where: { email } });
    }

    getByUsername(username) {
        return this.model.findOne({ where: { username } });
    }

    getUserById(id) {
        return this.model.findOne({
            group: [
                'user.id',
                'image.id'
            ],
            where: { id },
            include: {
                model: ImageModel,
                attributes: ['id', 'link']
            }
        });
    }

    async setRecoveryToken(email) {
        const resetPasswordToken = crypto.randomBytes(20).toString('hex');
        const result = await this.model.update({
            resetPasswordToken,
            resetPasswordTokenExpires: Date.now() + 360000
        }, {
            where: { email },
            returning: true,
            plain: true
        });

        return { resetPasswordToken: result[1].resetPasswordToken };
    }
}

export default new UserRepository(UserModel);
